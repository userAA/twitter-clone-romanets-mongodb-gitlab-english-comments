import express from 'express';

import { validationResult } from 'express-validator';
import { TweetModel } from '../models/TweetModel';
import { UserModel } from '../models/UserModel';
import { isValidObjectId } from "../utils/isValidObjectId";
import { UserModelInterface } from '../models/UserModel';

class TweetsController {
    //the function of receiving all tweets from database
    async index(_: express.Request, res: express.Response): Promise<void> {
        try {
            //receiving all tweets from database and all information for every tweet
            const tweets = await TweetModel.find({}).populate('user').sort({'createdAt': '-1'}).exec();

            res.json({
                status: 'success',
                data: tweets
            })
        } catch(error) {
            res.json({
                status: 'error',
                message: error
            })
        }
    }

    //the functuon of receiving tweet from database according to ID tweetId
    async show(req: express.Request, res: express.Response): Promise<void> {
        try {
            const tweetId = req.params.id;

            //checking the integrity of ID tweetId
            if (!isValidObjectId(tweetId)) {
                res.status(400).send();
                return;
            }

            //receiving tweet
            const tweet = await TweetModel.findById(tweetId).populate('user').exec();

            if (!tweet) {
                res.status(404).send();
                return;
            }

            res.json({
                status: 'success',
                data: tweet
            })

        } catch (error) {
            res.status(500).json({
                status: 'error',
                messsage: error
            })
        }    
    }

    //the function of receiving all tweets according to authorized user with ID userId
    async getUserTweets(req: any, res: express.Response): Promise<void> {
        try {
            const userId = req.params.id;

            //checking the integrity of ID userId
            if (!isValidObjectId(userId)) {
                res.status(400).send();
                return;
            }

            //receiving tweets according to authorized user with ID userId
            const tweet = await TweetModel.find({user: userId}).populate('user').exec();

            if (!tweet) {
                res.status(404).send();
                return;
            }

            res.json({
                status: 'success',
                data: tweet
            })

        } catch (error) {
            res.status(500).json({
                status: 'error',
                messsage: error
            })
        }    
    }

    //the function of creating new tweet in database of tweets, which is entered by an authorized user,
    //at the same time, a list of tweets IDs of an authorized user fills by ID of entered by him of tweet 
    async create (req: express.Request, res: express.Response): Promise<void> {

        try {   
            //define of authorized user and his ID
            const user = req.user as UserModelInterface;
            
            if (user?._id) {
                const errors = validationResult(req);

                if (!errors.isEmpty()) {
                    res.status(400).json({status: 'error', errors: errors.array()});
                    return;
                }

                //the information according to new tweet
                const data: any = {
                    text: req.body.text,
                    images: req.body.images,
                    user: user._id    
                }

                //fixing of added tweet
                const tweet = await TweetModel.create(data);

                //entering ID of added tweet in list of IDs tweets according to defined user which entered added tweet
                UserModel.findById(user?._id, (err:any, user:any) => 
                {
                    if (err) 
                    {
                        res.status(500).json
                        ({
                            status: "error",
                            message: err,
                        });
                    }
    
                    if (!user) 
                    {
                        return res.status(404).json
                        ({
                            status: "not found",
                            message: err,
                        });
                    }
                  
                    user.tweets!.push(tweet._id);
                    user.save();
                });


                res.json({
                    status: 'success',
                    data: await tweet.populate('user').execPopulate()
                });
            }
        }   catch (error) {
            res.status(500).json({
                status: 'error',
                message: error
            })
        }     
    }       

    //the function of removing any tweet from any user
    async delete (req: express.Request, res: express.Response): Promise<void> {
        try {
                //fixing ID of removing tweet
                const tweetId = req.params.id;

                //checking integrity of ID tweetId
                if (!isValidObjectId(tweetId)) {
                    res.status(400).send();
                    return;
                }

                //fixing of removing tweet
                const tweet = await TweetModel.findById(tweetId);
 
                if (tweet) {
                    //defining of user user which entered removing tweet
                    UserModel.findById(tweet.user._id, (err:any, user:any) => 
                    {
                        if (err) 
                        {
                            res.status(500).json
                            ({
                                status: "error",
                                message: err,
                            });
                        }
            
                        if (!user) 
                        {
                            return res.status(404).json
                            ({
                                status: "not found",
                                message: err,
                            });
                        }
                          
                        //we removing ID of removing tweet from list ID tweets accordng to 
                        //defined user which entered removing tweet
                        user.tweets!.remove(tweetId);
                        user.save();
                    });

                    //removing tweet itself from database of tweets
                    tweet.remove();
                } else {
                    res.status(404).send();       
                }
            
        } catch (error) {
            res.status(500).json({
                status: 'error',
                message: error
            })
        }
    }    
}

export const TweetsCtrl = new TweetsController();