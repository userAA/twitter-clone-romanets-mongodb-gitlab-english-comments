import { body } from 'express-validator';

//checking that maximum length of message in tweet must be 280 symbols
export const createTweetValidations = [
  body('text', 'Введите текст твита')
    .isString()
    .isLength({
        max: 280
    })
    .withMessage('Максимальная длина твита 280 символов')
];