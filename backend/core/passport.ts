import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';
import {Strategy as JWTstrategy, ExtractJwt} from "passport-jwt";
import {UserModel, UserModelInterface} from "../models/UserModel";

//password encryption function
import {generateMD5} from '../utils/generateHash'; 

passport.use(
    //checking if there is a user according to incoming data email and password, which is subject to authorization
    new LocalStrategy( async (email, password, done):Promise<void> => {
        try {
            //seeking user according to email
            const user = await UserModel.findOne({$or: [{email: email}]}).exec();

            if (!user){
                //there is no such user who needs to be authorized
                return done(null, false);
            } 

            if (!user.confirmed && user.password === generateMD5(password + process.env.SECRET_KEY)) {
                //there is such a user, we starting its authorization
                return done(null, user);
            } else {
                //there is no such user who needs to be authorized
                return done(null, false); 
            }
        } catch(error) {
            done(error, false);
        }
    })
)

passport.use(
    //checking if there is authorized user
    new JWTstrategy(
        {
            secretOrKey: process.env.SECRET_KEY || '123',
            jwtFromRequest: ExtractJwt.fromHeader('token')
        },
        async(payload: {data: UserModelInterface}, done): Promise<void> => {

            try {
                //believing that the authorized user has an ID payload.data._id we seeking such user in database
                const user = await UserModel.findById(payload.data._id).exec();

                if (user) {
                    //there is authorized user, we begining corresponding actions on all data, which relating to it
                    return done(null, user)
                }    
                
                //there is no such user
                done(null, false);
            } catch (error) {
                done(error, false);
            }
        }
    )
)

//serialization of the user by passport
passport.serializeUser((user: UserModelInterface, done) => {
    done(null, user?._id)
});

export {passport};