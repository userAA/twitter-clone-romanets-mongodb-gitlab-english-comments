import {mongoose} from "../core/db"

//the function of control integrity ID of element from data base MongoDb
export const isValidObjectId = (id: any) => {
    return mongoose.Types.ObjectId.isValid(id)
}