import {model, Schema, Document} from "mongoose"

//model user interface
export interface UserModelInterface {
    _id?: string,
    email: string;
    fullname: string;
    username: string;
    password: string;
    confirmHash: string;
    confirmed?: boolean;
    location?: string;
    about?: string;
    website?: string;
    tweets?: string[];
}

export type UserModelDocumentInterface = UserModelInterface & Document;

//user data model
const UserSchema = new Schema<UserModelInterface>({
    email: {
        unigue: true,
        required: true,
        type: String
    },
    fullname : {
        required: true,
        type: String
    },
    username: {
        unigue: true,
        required: true,
        type: String
    },
    password: {
        required: true,
        type: String
    },
    confirmHash: {
        required: true,
        type: String
    },
    confirmed: {
        type: Boolean,
        default: false
    },
    location: String,
    about: String,
    website: String,
    //full information on the user's tweets can be obtained for all 
    //the IDs of the list of user tweets IDs from the user tweets database 
    //are indicated by the ref: "Tweet" link
    tweets: [{type: Schema.Types.ObjectId, ref: "Tweet"}]
}, {
    timestamps: true    
})

UserSchema.set('toJSON', {
    transform: function(_: any, obj: any) {
        delete obj.password;
        delete obj.confirmHash;
        return obj;    
    }
})

//fixing database according to model data user in MongoDb
export const UserModel = model<UserModelDocumentInterface>('User', UserSchema);