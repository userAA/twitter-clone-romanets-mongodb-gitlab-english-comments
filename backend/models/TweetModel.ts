import {model, Schema, Document} from "mongoose"
import {UserModelDocumentInterface} from './UserModel';

//tweet model of user
export interface TweetModelInterface {
    _id?: string;
    text: string;
    user: UserModelDocumentInterface;
    images?: string[];
}

export type TweetModelDocumentInterface = TweetModelInterface & Document;

const TweetSchema = new Schema<TweetModelInterface>({
    text: {
        required: true,
        type: String,
        maxlength: 280
    },
    //the information about user one can receive from database of users this is indicated by the link ref: 'User'
    user: {
        required: true,
        ref: 'User',
        type: Schema.Types.ObjectId
    },
    images: [
        {
            type: String
        }
    ]
}, {
    timestamps: true
})

//fixing database according to model tweet user in MongoDb
export const TweetModel = model<TweetModelDocumentInterface>('Tweet', TweetSchema);