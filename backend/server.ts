import dotenv from 'dotenv';
dotenv.config();

import './core/db';

import express from 'express';
import multer from 'multer';
import bodyParser from 'body-parser';

import { registerValidations } from './validations/register';
import { passport } from './core/passport';
import { createTweetValidations } from './validations/createTweet';

import { TweetsCtrl } from './controllers/TweetsController';
import { UserCtrl } from './controllers/UserController';
import { UploadFileCtrl } from './controllers/UploadFileController';

const app = express();

const storage = multer.memoryStorage();
const upload = multer({ storage });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//initializing passport
app.use(passport.initialize());

//request for receiving information about authorized user by token, if there is a token 
app.get('/users/me', UserCtrl.getUserInfo);
//request for receiving all information about authorized user
app.get('/users/:id', UserCtrl.show);
//request for registration of new user
app.post('/auth/register', registerValidations, UserCtrl.create);
//request for authorization of a registered user
//first we checking by passport.authenticate('local') that
//the authorized user is already in the database
app.post('/auth/login', passport.authenticate('local'), UserCtrl.afterLogin);

//request for receiving all tweets from database
app.get('/tweets', TweetsCtrl.index);
//request for receiving tweet from database according to it ID
app.get('/tweets/:id', TweetsCtrl.show);
//request for receiving all tweets according to authorized user
app.get('/tweets/user/:id', TweetsCtrl.getUserTweets);
//request for removing any tweet from any user
app.delete('/tweets/:id', TweetsCtrl.delete);
//request for creating new tweet in database tweets, which  authorized user enters
//is there an authorized user, we check by passport.authenticate('jwt') 
app.post('/tweets', passport.authenticate('jwt'), createTweetValidations, TweetsCtrl.create);

//request according to loading images for tweets of user via cloudinary
app.post('/upload', upload.single('image'), UploadFileCtrl.upload);

//starting server
app.listen(8888, (): void => {
    console.log("SERVER RUNNING!")
});