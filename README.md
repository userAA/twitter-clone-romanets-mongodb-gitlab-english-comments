gitlab page https://gitlab.com/userAA/twitter-clone-romanets-mongodb-gitlab-english-comments.git
gitlab comment twitter-clone-romanets-mongodb-gitlab-english-comments

project twitter-clone-romanets-mongodb
technologies used on the frontend:
    @hookform/resolvers,
    @material-ui/core,
    @material-ui/icons,
    @material-ui/lab,
    @types/classnames,
    @types/node,
    @types/react,
    @types/react-dom,
    @types/react-redux,
    @types/react-router-dom,
    @types/yup,
    axios,
    classnames,
    date-fns,
    identity-obj-proxy,
    medium-zoom,
    react,
    react-dom,
    react-hook-form,
    react-redux,
    react-router-dom,
    react-scripts,
    redux,
    redux-saga,
    sass,
    sass-loader,
    typescript,
    yup;

technologies used on the backend:
    @material-ui/lab,
    @types/body-parser,
    @types/express,
    @types/jsonwebtoken,
    @types/mongoose,
    @types/multer,
    @types/passport,
    @types/passport-jwt,
    @types/passport-local,
    body-parser,
    cloudinary,
    crypto-js,
    dotenv,
    express,
    express-validator,
    jsonwebtoken,
    mongoose,
    multer,
    nodemon,
    passport,
    passport-jwt,
    passport-local,
    ts-node,
    typescript;
Authorized user creates tweets, which one can to add or to remove. Tweets consist of text or images.
Also home page displays a complete list of tweets for all users, including number and unauthorized.
One can to receive total information according to tweet simply clicking on him.
