import axios from 'axios';

//the template of getting data according to file with cloudinary
interface UploadImageReturnProps {
    height: number;
    size: number;
    url: string;
    width: number;
}

//request for creating of sample image of creating tweet in cloudinary
export const uploadImage = async (image: File): Promise<UploadImageReturnProps> => {
    const formData = new FormData();
    formData.append('image', image);  

    //we receiving data according to file with coudinary at outputing data according file formData
    const { data } = await axios.post('/upload', formData, {
        headers: {
            'Content-Type': 'multipart/form-data',
        }
    });
    return data;
}