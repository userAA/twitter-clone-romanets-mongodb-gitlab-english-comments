import {colors} from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import ArrowBottomIcon from '@material-ui/icons/KeyboardArrowDown';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import React from 'react';
import {useHomeStyles} from '../pages/theme';
import { useDispatch, useSelector } from 'react-redux';
import { selectUserData } from '../store/ducks/user/selectors';
import { Link } from "react-router-dom";
import { signOut } from '../store/ducks/user/actionCreators';

interface UserSideProfileProps {
    classes: ReturnType<typeof useHomeStyles>
}

export const UserSideProfile: React.FC<UserSideProfileProps> = ({ classes }: UserSideProfileProps) => {
    const dispatch = useDispatch();

    //we receiving data according to authorized user
    const userData = useSelector(selectUserData);

    //linking the context menu for displaying tweets by user or exiting the tweet page
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    //the function of opening context menu of showing tweets by user or exiting the tweet page
    const handleOpenPopup = (event: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
        setAnchorEl(event.currentTarget);
    };
    
    //the function of closing context menu of showing tweets by user or exiting the tweet page
    const handleClosePopup = (): void => {
        setAnchorEl(null);
    };

    const handleSignOut = () => {
        //we removing token, and at the same time we removing authorization from the user
        window.localStorage.removeItem('token');
        //we exiting from page Twitter
        dispatch(signOut())
    }

    //if there is no data on the authorized user then we exiting from this module
    if (!userData) {
        return null;
    }

    return (
        <>
            <div 
                // we opening context menu of represent tweet according to user or exiting from from page tweets
                onClick={handleOpenPopup} 
                className={classes.sideProfile}
            >
                {/*Avatar user*/}
                <Avatar/>
                <div className={classes.sideProfileInfo}>
                    {/*User full name*/}
                    <b>{userData.fullname}</b>
                    {/*User name*/}
                    <Typography style={{color: colors.grey[500]}}>@{userData.username}</Typography>
                </div>
                <ArrowBottomIcon/>
            </div>
            {/*context menu of represent tweet according to user or exiting from from pages tweets*/}
            <Menu 
                classes = {{
                    paper: classes.profileMenu
                }} 
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}         
                //we closing context menu of represent tweet according to user or exiting from pages tweets  
                onClose={handleClosePopup} 
                keepMounted
            >
                {/*Switching into page authorized user*/}
                <Link to={`/user/${userData._id}`}>
                    {/*We closing context menu*/}
                    <MenuItem onClick={handleClosePopup}>My profile</MenuItem>
                </Link>
                {/*We exiting from page tweets and removing authorization from the user, which generated the latest tweets */}
                <MenuItem onClick={handleSignOut}>Escape</MenuItem>
            </Menu>
        </>
    );
}