import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

//interface of modal window according to user registration or authorization 
interface ModalBlockProps {
  title?: string;
  children: React.ReactNode;
  visible?: boolean;
  onClose: () => void;
}

//modal window of user registration, authorization user or page of creating tweet by an authorized user 
export const ModalBlock: React.FC<ModalBlockProps> = ({
  //the name of modal window
  title,            
  //the function of closing modal window
  onClose,          
  //flag of visibility modal window
  visible = false,  
  //the components of content of modal window
  children          
}: ModalBlockProps): React.ReactElement | null => {
  if (!visible) {
    return null;
  }

  return (
    <Dialog open={visible} onClose={onClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">
         {/*the button of closing modal window*/}
        <IconButton onClick={onClose} color="secondary" aria-label="close">
          <CloseIcon style={{ fontSize: 26 }} color="secondary" />
        </IconButton>
        {/*Name of modal window */}
        {title}
      </DialogTitle>
       {/*We showing all components of modal window */}
      <DialogContent>{children}</DialogContent>
    </Dialog>
  );
};
