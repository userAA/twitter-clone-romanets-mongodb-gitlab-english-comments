import React from "react"
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useHistory } from "react-router";

export const BackButton: React.FC = (): React.ReactElement => {
    //hook of switching to web pages
    const history = useHistory();

    //the function of returning into previous page
    const handleClickButton = () => {
        history.goBack();
    }

    return (
        //the button of returning into previous page
        <IconButton onClick={handleClickButton} style={{marginRight: 20}} color="primary">
            <ArrowBackIcon/>
        </IconButton>
    )
}