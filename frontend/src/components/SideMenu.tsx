import React from 'react'
import TwitterIcon from '@material-ui/icons/Twitter';
import HomeIcon from '@material-ui/icons/HomeOutlined';
import UserIcon from '@material-ui/icons/PermIdentityOutlined';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

import { useHomeStyles } from '../pages/theme'
import { ModalBlock } from './ModalBlock';
import { AddTweetForm } from './AddTweetForm';
import { Link } from 'react-router-dom';
import { UserSideProfile } from './UserSideProfile';
import { useSelector } from 'react-redux';
import { selectUserData } from '../store/ducks/user/selectors';

interface SideMenuProps {
    classes: ReturnType<typeof useHomeStyles>;
}

//list actions in twitter
export const SideMenu: React.FC<SideMenuProps> = ({
    classes
}: SideMenuProps): React.ReactElement => {
    //the flag of visibility tweet formation page by an authorized user
    const [visibleAddTweet, setSetVisibleAddTweet] = React.useState<boolean>(false);
    //receiving all information about an authorized user
    const userData = useSelector(selectUserData);

    //the function of representing the tweet generation page by an authorized user
    const handleClickOpenAddTweet = () => {
        setSetVisibleAddTweet(true);
    }

    //the function of closing tweet generation page by an authorized user 
    const onCloseAddTweet = () => {
        setSetVisibleAddTweet(false);
    }

    return (
        <>
            <ul className={classes.sideMenuList}>
                <li className={classes.sideMenuListItem}>
                    {/*returning on main page */}
                    <Link to="/home">
                        <IconButton className={classes.logo}  aria-label="" color="primary">
                            <TwitterIcon className={classes.logoIcon} />
                        </IconButton>
                    </Link>
                </li>
                <li className={classes.sideMenuListItem}>
                    {/*returning on main page */}
                    <Link to="/home">
                        <div>
                            <HomeIcon className={classes.sideMenuListItemIcon}/>
                            <Typography className={classes.sideMenuListItemLabel} variant="h6">
                                Main
                            </Typography>
                        </div>
                    </Link>
                </li>
                <li className={classes.sideMenuListItem}>
                    {/*switching to page of showing full information about the authorized user, including his tweets */}
                    <Link to={`/user/${userData?._id}`}>
                        <div>        
                            <UserIcon className={classes.sideMenuListItemIcon}  />  
                            <Typography className={classes.sideMenuListItemLabel} variant="h6">
                                Profile
                            </Typography>      
                        </div>
                    </Link>
                </li>
                <li className={classes.sideMenuListItem}>
                    <Button 
                        //opening form of adding tweet
                        onClick={handleClickOpenAddTweet} 
                        className={classes.sideMenuTweetButton} 
                        variant="contained" 
                        color="primary" 
                        fullWidth
                    >
                        Tweet                        
                    </Button>
                    {/*Modal window of showing the form of tweet formation */}
                    <ModalBlock 
                        //we closing form of tweet formation
                        onClose={onCloseAddTweet} 
                        //flag of visibility of form tweet formation
                        visible={visibleAddTweet} 
                        title="" 
                    >
                        {/*Form of tweet formation */}
                        <div style={{width: 550}}>
                            <AddTweetForm maxRows={15} classes={classes}/>
                        </div>
                    </ModalBlock>
                </li>
            </ul>      
            {/*The actions with user data: to show his profile or remove it from authorization*/}  
            <UserSideProfile classes={classes}/>
        </> 
    )
}