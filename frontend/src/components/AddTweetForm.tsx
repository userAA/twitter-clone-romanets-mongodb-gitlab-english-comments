import React from 'react';
import classNames from 'classnames';
import Avatar from "@material-ui/core/Avatar";
import Alert from "@material-ui/lab/Alert";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import { useHomeStyles } from "../pages/theme";
import { useDispatch, useSelector } from 'react-redux';
import { fetchAddTweet, setAddFormState } from '../store/ducks/tweets/actionCreators';
import { selectAddFormState } from '../store/ducks/tweets/selectors';
import { AddFormState } from '../store/ducks/tweets/contracts/state';
import { UploadImages } from './UploadImages';
import { uploadImage } from '../utils/uploadImage';

interface AddTweetFormProps {
    classes: ReturnType<typeof useHomeStyles>;
    maxRows?: number;
}

//maximum length text in tweet
const MAX_LENGTH = 280;

//interface data according to image
export interface ImageObj {
    blobUrl: string;
    file: File;
}

export const AddTweetForm: React.FC<AddTweetFormProps> = ({
    classes,
    maxRows
}: AddTweetFormProps): React.ReactElement => {
    const dispatch = useDispatch();
    //text of creating tweet (maximum dimension of text tweet 280 symbols)
    const [text, setText] = React.useState<string>('');
    //images of creating tweet, if they are loaded
    const [images, setImages] = React.useState<ImageObj[]>([]);

    //the content of process formation of a tweet according to the form data
    const addFormState = useSelector(selectAddFormState);

    //what percentage of text characters have already been entered in relation to
    //maximum number symbols, which can be entered for the text of the generated tweet
    const textLimitPercent = (text.length / 280) * 100; 
    //how many characters of the text of the generated tweet can still be entered
    const textCount = MAX_LENGTH - text.length;

    const handleChangeTextArea = (e: React.FormEvent<HTMLTextAreaElement>): void => {
        if (e.currentTarget) {
            //fixing text tweet
            setText(e.currentTarget.value)
        }
    }

    const handleClickAddTweet = async (): Promise<void> => {
        let result = [];

        //tweet is being creating according to data form
        dispatch(setAddFormState(AddFormState.LOADING))
        
        //fixing images of tweet
        for (let i = 0; i < images.length; i++) {
            const file = images[i].file;
            //locating the image into cloudinary via server
            const {url} = await uploadImage(file);
            result.push(url);
        }
        
        //making up request for adding tweet according to it text and it images
        dispatch(fetchAddTweet({ text, images: result }));
        
        //after creating of specified request we reseting to zero the field of text tweet 
        setText('');
        
        //after carrying out of specified request we cleaning up the list of images tweet 
        setImages([]);
    };

    return (
        <div>
            <div className={classes.addFormBody}>
                {/*User avatar*/}
                <Avatar
                    className={classes.tweetAvatar}
                    alt={'User avatar UserAvatar'} 
                />
                {/*The field of creating text tweet */}
                <TextareaAutosize
                    onChange={handleChangeTextArea}
                    className={classes.addFormTextarea}
                    placeholder="What is going on?" 
                    value={text}
                    rowsMax={maxRows}
                />
            </div>
            <div className={classes.addFormBottom}>
                <div className={classNames(classes.tweetFooter, classes.addFormBottomActions)}>
                    {/*Uploading and generating of images for a tweet*/}
                    <UploadImages images={images} onChangeImages={setImages} />
                </div>
                <div className={classes.addFormBottomRight}>
                    {text && (
                        <>
                            {/*This diagram tells you, how many more characters can be included in the text for a tweet*/}
                            <span>{textCount}</span>
                            <div className={classes.addFormCircleProgress}>
                                <CircularProgress 
                                    variant="static"
                                    size={20}
                                    thickness={5}
                                    value={text.length >= MAX_LENGTH ? 100 : textLimitPercent}
                                    style={text.length >= MAX_LENGTH ? {color: 'red'} : undefined}
                                />
                                <CircularProgress 
                                    style={{color: 'rgba(0,0,0,0.1)'}}
                                    variant="static"
                                    size={20}
                                    thickness={5}
                                    value={100}
                                />
                            </div>
                        </>
                    )}
                    {/*Tweet generation button */}
                    <Button
                        onClick={handleClickAddTweet}
                        disabled={addFormState === AddFormState.LOADING || !text || text.length >= MAX_LENGTH}
                        color="primary"
                        variant="contained"
                    > 
                        {/*If data for tweet creating is prepared, then an authorized user can form a tweet */}
                        {addFormState === AddFormState.LOADING ? (
                            <CircularProgress color="inherit" size={16}/> 
                        ) : ( 
                            'Tweet'
                        )}
                    </Button>
                </div>
            </div> 
            { 
                addFormState === AddFormState.ERROR && (
                <Alert severity="error">
                    Error when adding a tweet{' '}
                    <span aria-label="emoji-plak" role="img">
                        😞
                    </span>
                </Alert>      
            )}
        </div>
    )
}