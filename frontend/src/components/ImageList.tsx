import React from 'react'
import {useHomeStyles} from '../pages/theme';
import {IconButton} from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';

interface ImageListProps {
    images: string[];
    classes: ReturnType<typeof useHomeStyles>;
    removeImage?: (url: string) => void;
}

//we outputing a list of images of the generated tweet, if there is one
export const ImageList: React.FC<ImageListProps> = ({classes,images,removeImage}) => {
    if (!images.length) {
        return null;
    }

    return (
        <div className={classes.imagesList}>
            {images.map((url) => {
                return (
                    <div className={classes.imagesListItem}>
                        {/*create button of removing image of creating tweet if such an image is set */}
                        {removeImage && (
                            <IconButton
                                className={classes.imagesListItemRemove}
                                onClick={(): void => removeImage(url)} 
                            >
                                <ClearIcon style={{fontSize: 15}}/>
                            </IconButton>
                        )}
                        {/*the image of creating tweet*/}
                        <img key={url} src={url}  alt=""/>
                    </div> 
                )
            })}
        </div>
    )
}