import React from 'react';
import classNames from 'classnames';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import { Avatar, IconButton, Menu, MenuItem, Paper, Typography } from '@material-ui/core';
import { useHomeStyles } from '../pages/theme';
import { useHistory } from 'react-router-dom';
import { formatDate } from '../utils/formatDate';
import { ImageList } from './ImageList';
import { removeTweet } from '../store/ducks/tweets/actionCreators';
import { useDispatch } from 'react-redux';

//the template of full data according to tweet
interface TweetProps {
  _id: string;
  text: string;
  classes: ReturnType<typeof useHomeStyles>;
  createdAt: string;
  images?: string[],
  user: {
    fullname: string;
    username: string;
    avatarUrl: string;
  };
}

export const Tweet: React.FC<TweetProps> = ({
  //ID tweet
  _id,     
  //text tweet   
  text,      
  //user, which this tweet created  
  user,      
  classes, 
  //tweet images
  images,   
  //the time of formation tweet
  createdAt   
}: TweetProps): React.ReactElement => {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  //hook switching into web pages
  const history = useHistory();

  //the function of receiving of full information about tweet into separate page
  const handleClickTweet = (event: React.MouseEvent<HTMLAnchorElement>): void => {
    event.preventDefault();
    //we go to the page showing separate information on the tweet with the identifier _id
    history.push(`/home/tweet/${_id}`);
  }

  //the function of appearing context menu actions with tweet
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    event.preventDefault();
    setAnchorEl(event.currentTarget);
  };

  //the function of closing context menu actions with tweet
  const handleClose = (event: React.MouseEvent<HTMLElement>):void => {
    event.stopPropagation();
    event.preventDefault();
    setAnchorEl(null);
  };

  //the function of removing tweet
  const handleRemove = (event: React.MouseEvent<HTMLElement>): void => {
    //we closing context menu of actions with tweet 
    handleClose(event); 
    if (window.confirm('Do you really want to delete a tweet?')) {
      //we removing tweet according to ID
      dispatch(removeTweet(_id));
    }
  };

  return (
    <a 
      //when clicking on the tweet itself we receiving full information according to tweet on web-page  /home/tweet/${_id}
      onClick={handleClickTweet} 
      className={classes.tweetWrapper} 
      href={`/home/tweet/${_id}`}
    >
      <Paper className={classNames(classes.tweet, classes.tweetsHeader)} variant="outlined">
        {/*General user avatar*/}
        <Avatar
          className={classes.tweetAvatar}
          alt={`User avatar ${user.fullname}`}
          src={user.avatarUrl}
        />
        <div className={classes.tweetContent}>
          <div className={classes.tweetHeader}>
            <div>
              {/*Full name user, who generated the tweet */}
              <b>{user.fullname}</b>&nbsp;
              {/*User name, who generated the tweet */}
              <span className={classes.tweetUserName}>@{user.username}</span>&nbsp;
              <span className={classes.tweetUserName}>·</span>&nbsp;
              {/*The time of formation tweet*/}
              <span className={classes.tweetUserName}>{formatDate(new Date(createdAt))}</span>
            </div>
            <div>
              {/*Appearing button of context menu of list actions with tweet */}
              <IconButton
                aria-label="more"
                aria-controls="long-menu"
                aria-haspopup="true"
                onClick={handleClick}
              >
                <MoreVertIcon />
              </IconButton>
              {/*The list actions with tweet, so far, only its removal*/}
              <Menu
                id="long-menu"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}
              >
                {/*Removing tweet */}
                <MenuItem onClick={handleRemove}>
                  Удалить твит
                </MenuItem>
              </Menu>
            </div>
          </div>
          <Typography variant="body1" gutterBottom>
            {/*The text of the tweet, if there is one */}
            {text}
            {/*Tweet images, if any */}
            { images && <ImageList classes={classes} images={images}/>}
          </Typography>
        </div>
      </Paper>
    </a>
  );
};