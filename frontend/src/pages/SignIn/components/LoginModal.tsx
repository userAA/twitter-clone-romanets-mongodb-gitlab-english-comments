import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import TextField from '@material-ui/core/TextField';
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import { useStylesSignIn } from '..';
import { ModalBlock } from '../../../components/ModalBlock';
import { fetchSignIn, setUserLoadingStatus } from '../../../store/ducks/user/actionCreators';
import { selectUserStatus } from '../../../store/ducks/user/selectors';
import { LoadingStatus } from '../../../store/types';

interface LoginModalProps {
    open: boolean;
    onClose: () => void;
}

//interface of data about authorized user
export interface LoginFormProps {
    email: string;
    password: string;
}

//information template for information about authorized user
const LoginFormSchema = yup.object().shape({
    email: yup.string().email('Invalid mail').required('Enter your email'),
    password: yup.string().min(6, '​​The minimum password length is 6 characters').required(),
});

export const LoginModal: React.FC<LoginModalProps> = ({ open, onClose }): React.ReactElement => {
    //we receiving layout styles
    const classes = useStylesSignIn();

    //status of information submission about authorized user
    const loadingStatus = useSelector(selectUserStatus);

    const { control, handleSubmit, formState: { errors } } = useForm<LoginFormProps>({
        resolver: yupResolver(LoginFormSchema)
    });

    const dispatch = useDispatch();
    const onSubmit = async (data: LoginFormProps) => {
        //we begining process of the selected registered user authorization 
        setUserLoadingStatus(LoadingStatus.LOADING);

        //we making up request into server for user authorization
        dispatch(fetchSignIn(data));

        //we closing dialog of user authorization
        onClose();
    };

    return (
        <ModalBlock
            visible={open}
            onClose={onClose}
            title="Log in to your account"
        >
            <form 
                //authorizing the user according to this function using button Authorization
                onSubmit={handleSubmit(onSubmit)}  
            >
                <FormControl className={classes.loginFormControl} component="fieldset" fullWidth>
                    <FormGroup aria-label="position" row>
                        {/*Email of the authorized user */}
                        <Controller
                            name="email"
                            control={control}
                            defaultValue=""
                            render={({ field } ) => 
                                <TextField
                                    {...field}
                                    className={classes.loginSideField} 
                                    id="email"
                                    label="E-Mail"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    variant="filled"
                                    type="email"
                                    helperText={errors.email?.message}
                                    error={!!errors.email}            
                                    fullWidth    
                                    autoFocus   
                                />
                            }
                        />
                        {/*Password of the authorized user */}
                        <Controller
                            name="password"
                            control={control}
                            defaultValue=""
                            render={({ field } ) => 
                                <TextField
                                    {...field}
                                    className={classes.loginSideField} 
                                    id="password"
                                    label="Password"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    variant="filled"
                                    type="password"
                                    helperText={errors.password?.message}
                                    error={!!errors.password}            
                                    fullWidth       
                                />
                            }
                        />
                        {/*when is the submission of information about authorized user this button not activated */}
                        <Button 
                            disabled={loadingStatus === LoadingStatus.LOADING} 
                            type="submit" 
                            variant="contained" 
                            color="primary" 
                            fullWidth
                        >
                            Enter
                        </Button>
                    </FormGroup>
                </FormControl>
            </form>
        </ModalBlock>
    )
}