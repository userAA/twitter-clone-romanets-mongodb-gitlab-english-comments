import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import TextField from '@material-ui/core/TextField';
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import { useStylesSignIn } from '..';
import { ModalBlock } from '../../../components/ModalBlock';
import {  fetchSignUp } from '../../../store/ducks/user/actionCreators';
import { selectUserStatus } from '../../../store/ducks/user/selectors';
import { LoadingStatus } from '../../../store/types';

interface RegisterModalProps {
    open: boolean;
    onClose: () => void;
}

//data interface about new the registered user
export interface RegisterFormProps {
    fullname: string;
    username: string;
    email: string;
    password: string;
    password2: string;
}

//information template for information about new registered user
const RegisterFormSchema = yup.object().shape({
    fullname: yup.string().required('Enter your name'),
    email: yup.string().email('Invalid email').required('Enter email'),
    username: yup.string().required('Invalid login'),
    password: yup.string().min(6, '​​​The minimum password length is 6 characters').required(),
    password2: yup.string().oneOf([yup.ref('password')], '​Passwords do not match'),
});

export const RegisterModal: React.FC<RegisterModalProps> = ({ open, onClose }): React.ReactElement => {
    //we obtaining the layout styles
    const classes = useStylesSignIn();
    
    //the status of transmission of information about user into server
    const loadingStatus = useSelector(selectUserStatus);

    const { control, handleSubmit, formState: { errors } } = useForm<RegisterFormProps>({
        resolver: yupResolver(RegisterFormSchema)
    });

    const dispatch = useDispatch();
    const onSubmit = async (data: RegisterFormProps) => {
        //making up request into server for a new registered user
        dispatch(fetchSignUp(data));
        //closing the window of user registration
        onClose();
    };

    return  (
        <ModalBlock
            visible={open}
            onClose={onClose}
            title="Create account"
        >
            <form 
                //registering user according to this function using the Registration button
                onSubmit={handleSubmit(onSubmit)} 
            >
                <FormControl className={classes.loginFormControl} component="fieldset" fullWidth>
                    <FormGroup aria-label="position" row>
                        {/*Email of the new registered user*/}
                        <Controller
                            name="email"
                            control={control}
                            defaultValue=""
                            render={({ field } ) => 
                                <TextField
                                    {...field}
                                    className={classes.registerField} 
                                    id="email"
                                    label="E-Mail"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    variant="filled"
                                    type="email"
                                    helperText={errors.email?.message}
                                    error={!!errors.email}            
                                    fullWidth    
                                    autoFocus   
                                />
                            }
                        />       
                        {/*Login of the new registered user*/}
                        <Controller
                            name="username"
                            control={control}
                            defaultValue=""
                            render={({ field } ) => 
                                <TextField
                                    {...field}
                                    className={classes.registerField} 
                                    id="username"
                                    label="Login"
                                    InputLabelProps={{
                                    shrink: true,
                                    }}
                                    variant="filled"
                                    type="text"
                                    helperText={errors.username?.message}
                                    error={!!errors.username}            
                                    fullWidth       
                                />
                            }
                        />
                        {/*Name of the new registered user*/}
                        <Controller
                            name="fullname"
                            control={control}
                            defaultValue=""
                            render={({ field } ) => 
                                <TextField
                                    {...field}
                                    className={classes.registerField} 
                                    id="fullname"
                                    label="Our name"
                                    InputLabelProps={{
                                    shrink: true,
                                    }}
                                    variant="filled"
                                    type="text"
                                    helperText={errors.fullname?.message}
                                    error={!!errors.fullname}            
                                    fullWidth       
                                />
                            }
                        />
                        {/*Password of the new registered user*/}
                        <Controller
                            name="password"
                            control={control}
                            defaultValue=""
                            render={({ field } ) => 
                                <TextField
                                    {...field}
                                    className={classes.registerField} 
                                    id="password"
                                    label="Password"
                                    InputLabelProps={{
                                    shrink: true,
                                    }}
                                    variant="filled"
                                    type="password"
                                    helperText={errors.password?.message}
                                    error={!!errors.password}            
                                    fullWidth       
                                />
                            }
                        />
                        {/*Repeating of a new registered user password*/}
                        <Controller
                            name="password2"
                            control={control}
                            defaultValue=""
                            render={({ field } ) => 
                                <TextField
                                    {...field}
                                    className={classes.registerField} 
                                    id="password2"
                                    label="Password"
                                    InputLabelProps={{
                                    shrink: true,
                                    }}
                                    variant="filled"
                                    type="password"
                                    helperText={errors.password?.message}
                                    error={!!errors.password}            
                                    fullWidth       
                                />
                            }
                        />
                        {/*when is the submission of information about new authorized user this button not activated */}
                        <Button 
                            disabled={loadingStatus === LoadingStatus.LOADING} 
                            type="submit" 
                            variant="contained" 
                            color="primary" 
                            fullWidth
                        >
                            Registration
                        </Button>
                    </FormGroup>
                </FormControl>
            </form>
        </ModalBlock>
    )
}