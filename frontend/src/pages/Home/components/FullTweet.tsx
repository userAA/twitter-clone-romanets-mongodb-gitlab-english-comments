import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import mediumZoom from 'medium-zoom';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames'; 

import {Divider} from '@material-ui/core';

import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import format from 'date-fns/format';
import ruLang from 'date-fns/locale/ru';

import { fetchTweetData } from '../../../store/ducks/tweet/actionCreators';
import { selectIsTweetLoading, selectTweetData } from '../../../store/ducks/tweet/selectors';
import { useHomeStyles } from '../../theme';

import { ImageList } from '../../../components/ImageList';

export const FullTweet: React.FC = (): React.ReactElement | null => {
    const classes = useHomeStyles();
    const dispatch = useDispatch();

    //receiving ID tweet
    const params: { id?: string } = useParams();
    const id = params.id;

    //receiving full tweet with ID id
    const tweetData = useSelector(selectTweetData);
    
    //flag of downloading tweet with ID id from server
    const isLoading = useSelector(selectIsTweetLoading);

    React.useEffect(() => {
        //if ID of tweet is defined then we making up request on it downloading with server
        if (id) {
            dispatch(fetchTweetData(id));
        }
    }, [dispatch, id]);

    //we less increasing the image of tweet if does not implement process of tweet downloading with server 
    React.useEffect(() => {
        if (!isLoading) {
          mediumZoom('.tweet-images img');
        }
    }, [isLoading]);
    
    //fixing process of downloading tweet with ID id from server
    if (isLoading) {
        return (
            <div className={classes.tweetsCentred}>
                <CircularProgress />
            </div>
        );
    }

    //we display the full information about the tweet
    if (tweetData) {
        return (
            <>
                <Paper className={classes.fullTweet}>
                    <div  className={classNames(classes.tweetsHeaderUser)}>
                        {/*Avatar tweet*/}
                        <Avatar 
                            className={classes.tweetAvatar}
                            alt={`Аватарка пользователя ${tweetData.user.fullname}`} 
                            src={tweetData.user.avatarUrl}
                        />
                        <Typography>
                            {/*Full name user of tweet*/}
                            <b>{tweetData.user.fullname}</b>&nbsp;
                            <div>
                                {/*name user of tweet*/}
                                <span className={classes.tweetUserName}>@{tweetData.user.username}</span>&nbsp;
                            </div>
                        </Typography>
                    </div>
                    <Typography className={classes.fullTweetText} gutterBottom>
                        {/*Text of tweet*/}
                        {tweetData.text}
                        {/*All images of tweet if there are any*/}
                        <div className="tweet-images">
                            {tweetData.images && <ImageList classes={classes} images={tweetData.images}/>}
                        </div>
                    </Typography>
                    <Typography>
                        {/*Time creating of tweet on hour and minutes*/}
                        <span className={classes.tweetUserName}>{format(new Date(tweetData.createdAt), 'H:mm', {locale: ruLang})} . </span>
                        {/*Time creating of tweet on days month and years*/}
                        <span className={classes.tweetUserName}>{format(new Date(tweetData.createdAt), 'dd MMM. yyyy  г.', {locale: ruLang})} </span>
                    </Typography>
                </Paper>
                <Divider/>            
            </>
        )
    }    
    return null;
};
