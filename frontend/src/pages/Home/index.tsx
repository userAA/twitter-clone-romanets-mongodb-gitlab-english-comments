import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress'; 

import { AddTweetForm } from '../../components/AddTweetForm';
import {Tweet} from '../../components/Tweet';
import { useHomeStyles } from '../theme';
import { useDispatch, useSelector } from "react-redux";
import { fetchTweets } from '../../store/ducks/tweets/actionCreators';
import { selectIsTweetsLoading, selectTweetsItems} from '../../store/ducks/tweets/selectors';
import { Route } from 'react-router-dom';
import { BackButton } from '../../components/BackButton';
import { FullTweet } from './components/FullTweet';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

//the page of creating tweets
export const Home = (): React.ReactElement => {
    const classes = useHomeStyles();
    const dispatch = useDispatch();
    //downloading of all tweets with server what are
    const tweets = useSelector(selectTweetsItems);
    //we definiig flag of process downloading of all tweets with server
    const isLoading = useSelector(selectIsTweetsLoading);

    React.useEffect(() => {
        //makeing up request for downloading  of all tweets from server
        dispatch(fetchTweets())
    }, [dispatch])

    return (
        <Paper className={classes.tweetsWrapper} variant="outlined">
            <Paper className={classes.tweetsHeader} variant="outlined">
                {/*Появление кнопки возвращения на предыдущую страницу */}
                {/*The appearance of the button to return to the previous page */}
                <Route  path="/home/:any" >
                    <BackButton/>
                </Route>
                            
                <Route  path={'/home'} exact>
                    <Typography variant="h6">Tweets</Typography>
                </Route>
            </Paper>
            
            <Route path={'/home'} exact>
                <Paper>
                    {/*The form of adding tweet*/}
                    <div className={classes.addForm}>
                        <AddTweetForm classes={classes} />
                    </div>
                    <div className={classes.addFormBottomLine} />
                </Paper>
            </Route>
            
            <Route path="/home" exact>
                {/*We fixing process of downloading tweets with server*/}
                {isLoading ? (
                    <div className={classes.tweetsCentred}>
                        <CircularProgress/>
                    </div>
                ) : (
                    //we outputing the list of all downloaded tweets with server 
                    tweets.map((tweet) => <Tweet key={tweet._id} classes={classes} images={tweet.images} {...tweet} />) 
                )}
            </Route>

            {/*Full information about every tweet at transmition into web-page /home/tweet/:id*/}
            <Route path="/home/tweet/:id" component={FullTweet} exact/>
        </Paper>                                                
    )
}