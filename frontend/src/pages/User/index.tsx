import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress'; 

import classNames from "classnames";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { Avatar } from '@material-ui/core';
import { BackButton} from '../../components/BackButton';
import { useHomeStyles } from '../theme';

import Sceleton from "@material-ui/lab/Skeleton";

import './User.scss';
import { useDispatch, useSelector } from 'react-redux';
import { selectIsTweetsLoading, selectTweetsItems } from '../../store/ducks/tweets/selectors';
import { Tweet } from '../../components/Tweet';
import { fetchTweets } from '../../store/ducks/tweets/actionCreators';

import { User } from '../../store/ducks/user/contracts/state';
import { AuthApi } from '../../services/api/authApi';

export const UserPage = () => {
    const classes = useHomeStyles();
    //we receiving tweets of authorized user from store if they have already been downloaded from the server
    const tweets = useSelector(selectTweetsItems);
    const dispatch = useDispatch();
    //flag progress of the downloading tweets process
    const isLoading = useSelector(selectIsTweetsLoading);
    //data authorized user and their template
    const [userData, setUserData] = React.useState<User | undefined>();

    React.useEffect(() => {
        //we receiving ID of authorized user 
        const userId = window.location.pathname.split('/').pop();

        //we receiving tweets of authorized user 
        dispatch(fetchTweets());

        if (userId) {
            //downloading with server all information about authorized user
            AuthApi.getUserInfo(userId).then(({data}) => {
                //we creating the data of the authorized user according to their template
                setUserData(data);    
            })
        }
    }, [dispatch])

    return (
        <Paper className={classNames(classes.tweetsWrapper,'user')} variant="outlined">
            <Paper className={classes.tweetsHeader} variant="outlined">
                {/*We returning into the previous web page */}
                <BackButton/>
                <div>
                    {/*My name*/}
                    <Typography variant="h6">Aleksandr Romanets</Typography>
                    {/*How many tweets does an authorized user have*/}
                    <Typography variant="caption" display="block" gutterBottom>
                        {tweets.length} tweets
                    </Typography>
                </div>
            </Paper>
            <div className="user__header"></div>

            <div className="user__info">
                {/*Avatar*/}
                <Avatar/>
                {
                    //full name of authorized user 
                    !userData ? <Sceleton variant="text" width={250} height={30} /> :
                    <h2 className="user__info-fullname">{userData?.fullname}</h2>
                }
                {
                    //name of authorized user 
                    !userData ? <Sceleton variant="text" width={60}/> :
                    <span className="user__info-username">@{userData?.username}</span>
                }
                <p className="user__info-description">
                    frontend Developer / UI Designer / JavaScript Red Heart ReactJS * React Native, NodeJS
                </p>
                {/*My personal information*/}
                <ul className="user__info-details">
                    <li>Moskow, Russian</li>
                    <li>
                        <a className="link" href="https://vk.com/im">
                            romanets.aa
                        </a>
                    </li>
                    <li>
                        <br/>
                    </li>
                    <li>Date of birth: 29 december 1980 г.</li>
                    <li>Registration: november 2021г.</li>
                </ul>
            </div>

            <div className="user__tweets">
                {isLoading ? (
                    //we tracking the status of the downloading process of all tweets of authorized user 
                    <div className={classes.tweetsCentred}>
                        <CircularProgress/>
                    </div>
                ) : (
                    //we outputing all tweets of authorized user 
                    tweets.map((tweet) => {
                    return (
                        <Tweet key={tweet._id} classes={classes} images={tweet.images} {...tweet} />
                    )})
                )}
            </div>
        </Paper>
    )
} 