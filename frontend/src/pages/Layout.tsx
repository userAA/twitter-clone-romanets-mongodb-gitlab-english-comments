import React from 'react';
import { Container, Grid } from '@material-ui/core';

import { SideMenu } from '../components/SideMenu';
import { useHomeStyles } from './theme';

interface layout {
  children: React.ReactNode;
}

//the template of central page when the user is logged in
export const Layout: React.FC<layout> = ({ children }): React.ReactElement => {
    const classes = useHomeStyles();

    return (
        <Container className={classes.wrapper} maxWidth="lg">
            <Grid container spacing={3}>
                <Grid sm={1} md={3} item>
                    {/*List of actions with tweets*/}                   
                    <SideMenu classes={classes} />
                </Grid> 
                {/*Illustration of tweets and actions with them (creation, deletion)*/}  
                <Grid sm={11} md={9} item>
                    {children}
                </Grid>
            </Grid>
        </Container>
    );
};
