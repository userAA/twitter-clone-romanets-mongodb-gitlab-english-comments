import {applyMiddleware, compose, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import { TweetsState } from './ducks/tweets/contracts/state';

//we importing root reducer
import {rootReducer} from './rootReducer'; 
//we importing root sagas
import rootSaga from './saga';            
import { TweetState } from './ducks/tweet/contracts/state';
import { UserState } from './ducks/user/contracts/state';

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const sagaMiddleware = createSagaMiddleware()

//the root of state data
export interface RootState {
    tweets: TweetsState;
    tweet: TweetState;
    user: UserState;
}

//download reducer, which in root reducer rootReducer
export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(sagaMiddleware)));

//download of root sagas
sagaMiddleware.run(rootSaga);