export enum LoadingStatus {
  //data already was uploaded or downloaded
  LOADED = 'LOADED',   
  //data are being uploaded or are being downloaded
  LOADING = 'LOADING',  
  //data are uploaded with error or are downloaded with error
  ERROR = 'ERROR',      
  //the data was never uploaded or never downloaded
  NEVER = 'NEVER',      
  //data are uploaded sucessfully or downloaded sucessfully
  SUCCESS = 'SUCCESS'   
}
  