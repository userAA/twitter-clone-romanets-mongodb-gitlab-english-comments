import {combineReducers} from "redux";
import { tweetsReducer } from "./ducks/tweets/reducer";
import { tweetReducer } from "./ducks/tweet/reducer";
import { userReducer } from "./ducks/user/reducer";

//root reducer
export const rootReducer = combineReducers({
    //reducer of tweets
    tweets: tweetsReducer, 
    //reducer of tweet
    tweet: tweetReducer,   
    //reducer of users
    user: userReducer      
}) 