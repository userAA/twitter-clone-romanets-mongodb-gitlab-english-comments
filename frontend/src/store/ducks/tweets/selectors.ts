import { RootState } from '../../store';
import {LoadingStatus} from '../../types';
import { AddFormState, TweetsState } from './contracts/state';

//we receiving all information about tweets from store via reducer
export const selectTweetsState = (state: RootState): TweetsState => state.tweets;

//we checking process of downloading all tweets from server
export const selectLoadingState = (state: RootState): LoadingStatus =>
  selectTweetsState(state).loadingState;

export const selectAddFormState = (state: RootState): AddFormState =>
  selectTweetsState(state).addFormState;

//we checking is being downloaded or no all tweets with server
export const selectIsTweetsLoading = (state: RootState): boolean =>
  selectLoadingState(state) === LoadingStatus.LOADING;

//we checking was downloaded or no all tweets with server 
export const selectIsTweetsLoaded = (state: RootState): boolean =>
  selectLoadingState(state) === LoadingStatus.LOADED;

//we receiving the tweets themselves
export const selectTweetsItems = (state: RootState) => selectTweetsState(state).items;