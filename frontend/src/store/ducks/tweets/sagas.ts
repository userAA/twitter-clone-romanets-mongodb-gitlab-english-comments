import { call, put, takeLatest } from 'redux-saga/effects';
import { TweetsApi } from '../../../services/api/tweetsApi';
import { 
  addTweet,  
  setAddFormState,  
  setTweets, 
  setTweetsLoadingState 
} from './actionCreators';
import {LoadingStatus} from '../../types';
import { FetchAddTweetActionInterface, RemoveTweetActionInterface, TweetsActionsType} from './contracts/actionTypes';
import { AddFormState } from './contracts/state';

//the function of implementing a download request for either all tweets or tweets
//that are related to an authorized user
export function* fetchTweetsRequest():any {
  try {
    const pathname = window.location.pathname;
    //ID of authorized user, if it can be determined by pathname, otherwise there is no ID
    const userId = pathname.includes('/user') ? pathname.split('/').pop() : undefined;
    //or we downloading all tweets, or tweets, which relevant to the authorized user
    const items = yield call(TweetsApi.fetchTweets, userId);
    //we fixing downloaded tweets
    yield put(setTweets(items));
  } catch (error) {
    //the tweets could not be downloaded
    yield put(setTweetsLoadingState(LoadingStatus.ERROR));
  }
}

//the function of request implementation for creating new tweet according to authorized user
export function* fetchAddTweetRequest({payload}: FetchAddTweetActionInterface):any {
  try {
    //creating new tweet item according to authorized user
    const item = yield call(TweetsApi.addTweet, payload);
    //we fixing new created tweet item according to authorized user
    yield put(addTweet(item));
  } catch (error) {
    //to create new tweet according to authorized user didn't work out
    yield put(setAddFormState(AddFormState.ERROR));
  }
}

//the function of realization request for removing tweet according to it ID from server
export function* fetchRemoveTweetRequest({payload}: RemoveTweetActionInterface):any {
  try {
    //we removing tweet according it ID payload from server
    yield call(TweetsApi.removeTweet, payload);
  } catch (error) {
    //the tweet could not be deleted
    alert('The error at removing tweet')
  }
}

export function* tweetsSaga() {
  //we making up request for downloading of tweets from server
  yield takeLatest(TweetsActionsType.FETCH_TWEETS, fetchTweetsRequest);
  //we making up request for forming of new tweet according to authorized user
  yield takeLatest(TweetsActionsType.FETCH_ADD_TWEET, fetchAddTweetRequest);
  //we making request for removing of tweet according to it ID
  yield takeLatest(TweetsActionsType.REMOVE_TWEET, fetchRemoveTweetRequest);
}
