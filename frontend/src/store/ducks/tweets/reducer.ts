import produce, { Draft } from 'immer';
import { TweetsActions } from './actionCreators';
import {TweetsActionsType} from './contracts/actionTypes';
import {LoadingStatus} from '../../types';
import {AddFormState,  TweetsState } from './contracts/state';

//initial state
const initialTweetsState: TweetsState = {
  //the list of downloaded tweets at first it is empty
  items: [], 
  //the state of creating tweet according data on form, at first there is no data on the form and the tweet is not formed
  addFormState: AddFormState.NEVER,
  //downloading of tweets with server, initially nothing is loaded into the server and nothing is downloaded from the server
  loadingState: LoadingStatus.NEVER, 
};

export const tweetsReducer = produce((draft: Draft<TweetsState>, action: TweetsActions) => {
  switch (action.type) {
    //we finding downloaded tweets from store at the same time we fixing the state of process downloading tweets in LOADED
    case TweetsActionsType.SET_TWEETS:
      draft.items = action.payload;
      draft.loadingState = LoadingStatus.LOADED;
      break;

    //we downloading the tweets from server there are no tweets in store
    case TweetsActionsType.FETCH_TWEETS:
      draft.items = [];
      draft.loadingState = LoadingStatus.LOADING;
      break;

    //we fixing the state of downloading tweets from server or loading tweets into server
    case TweetsActionsType.SET_LOADING_STATE:
      draft.loadingState = action.payload;
      break;

    //we looking how is being formed new tweet according data on form
    case TweetsActionsType.SET_ADD_FORM_STATE:
      draft.addFormState = action.payload;
      break;

    //we adding new tweet according to an authorized user into server, in store there is no such tweet yet
    case TweetsActionsType.FETCH_ADD_TWEET:
      //tweet forms according to data on form
      draft.addFormState = AddFormState.LOADING;
      break;

    //we removing tweet according to it ID from available list of tweets
    case TweetsActionsType.REMOVE_TWEET:
      draft.items = draft.items.filter((obj) => obj._id !== action.payload);
      break;

    //finding new tweet according to authorized user from store and we putting it on the first place of the entire list of tweets
    case TweetsActionsType.ADD_TWEET:
      draft.items.splice(0,0,action.payload);
      //tweet was formed according to data on form
      draft.addFormState = AddFormState.NEVER;
      break;

    default:
      break;
  }
}, initialTweetsState);
