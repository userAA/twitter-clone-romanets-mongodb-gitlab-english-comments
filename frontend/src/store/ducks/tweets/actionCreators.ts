import {
  //the data interface of a new tweet in the store
  AddTweetActionInterface, 
  //the data interface for creating new tweet on server according to authorized user
  FetchAddTweetActionInterface, 
  //interface of request for downloading tweets with server or all, or according to ID of separate user
  FetchTweetsActionInterface, 
  //interface of state forming tweet according to data on form
  SetAddFormStateActionInterface, 
  //interface of request for removing tweet according to it ID
  RemoveTweetActionInterface, 
  //interface of collection tweets
  SetTweetsActionInterface,
  //interface of state downloading tweets with server
  SetTweetsLoadingStateActionInterface, 
  //the types of actions with tweets
  TweetsActionsType 
} from './contracts/actionTypes';
import {LoadingStatus} from '../../types';
import { AddFormState,  Tweet, TweetsState } from './contracts/state';

//we fixing downloaded tweets at sending to store
export const setTweets = (payload: TweetsState['items']): SetTweetsActionInterface => ({
  //by this type we will receive a collection of tweets from the store
  type: TweetsActionsType.SET_TWEETS,
  payload,
});

//we switching into sagas the creating request for adding tweet
export const fetchAddTweet = (payload: {
  text: string, 
  images: string[]
}): FetchAddTweetActionInterface => ({
  type: TweetsActionsType.FETCH_ADD_TWEET,
  payload
});

//we fixing received from sagas new tweet in store according to authorized user
export const addTweet = (payload: Tweet): AddTweetActionInterface => ({
  //according to this type we receiving new tweet from store via reducer
  type: TweetsActionsType.ADD_TWEET, 
  payload,
});

//we fixing into store the state of downloading tweets from server
export const setTweetsLoadingState = ( payload: LoadingStatus): SetTweetsLoadingStateActionInterface => ({
  type: TweetsActionsType.SET_LOADING_STATE,
  payload
});

//we fixing into store the state of creating tweet according to data on form
export const setAddFormState = ( payload: AddFormState): SetAddFormStateActionInterface => ({
  type: TweetsActionsType.SET_ADD_FORM_STATE,
  payload
});

//we sending request into sagas for removing tweet according it ID payload
export const removeTweet = (payload: string): RemoveTweetActionInterface => ({
  type: TweetsActionsType.REMOVE_TWEET,
  payload
});

//we sending into sagas the creating of request about downloading tweets with server
export const fetchTweets = (): FetchTweetsActionInterface => ({
  //by this type
  type: TweetsActionsType.FETCH_TWEETS
});

//we combining all interfaces of data according to tweets for reducer
export type TweetsActions =
  | SetTweetsActionInterface
  | FetchTweetsActionInterface
  | SetTweetsLoadingStateActionInterface
  | FetchAddTweetActionInterface
  | AddTweetActionInterface
  | SetAddFormStateActionInterface
  | RemoveTweetActionInterface;
