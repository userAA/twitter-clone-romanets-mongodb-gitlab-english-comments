import {LoadingStatus} from '../../../types';

//the state of creating tweet on form
export enum AddFormState {
  //creating form of tweet
  LOADING = 'LOADING', 
  //the error in creating of form tweet
  ERROR = 'ERROR',     
  //either the tweet was formed from the data on the form or it was not formed
  NEVER = 'NEVER',     
}

//tweet interface
export interface Tweet {
  _id: string;
  text: string;
  images?: string[];
  createdAt: string;
  //user, which included tweet
  user: { 
    fullname: string;
    username: string;
    avatarUrl: string;
  };
}

//interface according to data about tweet including status of them downloading
export interface TweetsState {
  //list tweets
  items: Tweet[]; 
  //the state of downloading tweets from server
  loadingState: LoadingStatus; 
  //the state of creating tweets according to data on form
  addFormState: AddFormState; 
}