import {Action} from 'redux';
import {AddFormState,  Tweet, TweetsState} from './state';
import {LoadingStatus} from '../../../types';

export enum TweetsActionsType {
    //the type of receiving collection of tweets from store
    SET_TWEETS = 'tweets/SET_TWEETS',                
    //the type of downloading tweets from server
    FETCH_TWEETS = 'tweets/FETCH_TWEETS',            
    //the type of receiving of state downloading tweets from server
    SET_LOADING_STATE = 'tweets/SET_LOADING_STATE', 
    //the type of request for creating new tweet
    FETCH_ADD_TWEET = 'tweets/FETCH_ADD_TWEET',    
    //the type of receiving new tweet from store
    ADD_TWEET = 'tweets/ADD_TWEET',                  
    //the type of request for removing tweet according to it ID
    REMOVE_TWEET = 'tweets/REMOVE_TWEET',            
    //the type of receiving of state creating tweet according to data on form
    SET_ADD_FORM_STATE = 'tweets/SET_ADD_FORM_STATE'
}
 
//interface of tweet collection
export interface SetTweetsActionInterface extends Action<TweetsActionsType> {
    type: TweetsActionsType.SET_TWEETS;
    payload: TweetsState['items'];
}
  
//the interface of data for forming new tweet on server according to authorized user
export interface FetchAddTweetActionInterface extends Action<TweetsActionsType> {
    type: TweetsActionsType.FETCH_ADD_TWEET;
    payload: {
        text: string;
        images: string[];
    };
}
  
//the interface of new tweet data in store
export interface AddTweetActionInterface extends Action<TweetsActionsType> {
    type: TweetsActionsType.ADD_TWEET;
    payload: Tweet;
}

//interface of request for removing tweet according to it ID
export interface RemoveTweetActionInterface extends Action<TweetsActionsType> {
    type: TweetsActionsType.REMOVE_TWEET;
    payload: string;
}
  
//interface of request for downloading of tweets from server or all, or according ID of separate user
export interface FetchTweetsActionInterface extends Action<TweetsActionsType> {
    type: TweetsActionsType.FETCH_TWEETS;
}

//interface of state downloading tweets from server
export interface SetTweetsLoadingStateActionInterface extends Action<TweetsActionsType> {
    type: TweetsActionsType.SET_LOADING_STATE;
    //the state of downloading tweets with server
    payload: LoadingStatus; 
}
 
//interface of state forming tweet according to data on form
export interface SetAddFormStateActionInterface extends Action<TweetsActionsType> {
    type: TweetsActionsType.SET_ADD_FORM_STATE;
    //the state of creating tweet according to data on form
    payload: AddFormState;
}