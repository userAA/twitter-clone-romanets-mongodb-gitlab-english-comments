import produce, { Draft } from 'immer';
import {LoadingStatus} from '../../types';
import {  UserActions } from './actionCreators';
import {UserActionsType} from './contracts/actionTypes';
import {UserState} from './contracts/state';

const initialUserState: UserState = {
  //at initial time no data about user
  data: undefined,           
  //at initial time no any actions with data about users    
  status: LoadingStatus.NEVER   
};

export const userReducer = produce((draft: Draft<UserState>, action: UserActions) => {
  switch (action.type) {
    case UserActionsType.SET_USER_DATA:
      //getting data about user (will receive from ./selectors.ts)
      draft.data = action.payload;           
      //data about user were successfully received (we knowing about this status from ./selectors.ts)
      draft.status = LoadingStatus.SUCCESS;   
      break;

    case UserActionsType.SET_LOADING_STATE:
      //status of creating data about user (we knowing about this status from ./selectors.ts)
      draft.status = action.payload;          
      break;

    case UserActionsType.SIGN_OUT:
      //data about any user were created (know about this status from ./selectors.ts)
      draft.status = LoadingStatus.LOADED; 
      //data about authorized user were erased
      draft.data = undefined;              
      break;

    default:
      break;
  }
}, initialUserState);
