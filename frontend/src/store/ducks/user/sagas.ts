import { call, put, takeLatest } from 'redux-saga/effects';
import { AuthApi } from '../../../services/api/authApi';
import {
  //type of user data generation status
  LoadingStatus 
} from '../../types';
import {
  //function of transmition data about an authorized user into reducer
  setUserData,        
  //the function of setting the status of creating user data
  setUserLoadingStatus 
} from './actionCreators';
import { 
  //interface according to data user authorization
  FetchSignInActionInterface, 
  //interface according to data of a new user registration
  FetchSignUpActionInterface,
  //types of actions with user data
  UserActionsType            
} from './contracts/actionTypes';

//the request function for registration new user
export function* fetchSignUpRequest( { payload }: FetchSignUpActionInterface):any {   
  try {
    //the flag submission of data about a new registered user
    yield put(setUserLoadingStatus(LoadingStatus.LOADING));
    //the request submission of data about a new registered user
    yield call(AuthApi.signUp, payload);
    //the flag speak about that new the user has registered
    yield put(setUserLoadingStatus(LoadingStatus.SUCCESS));
  } catch (error) {
    //the flag speak about that new the user has no registered
    yield put(setUserLoadingStatus(LoadingStatus.ERROR));
  }
}

//the function of request for authorization of a new user
export function* fetchSignInRequest( { payload }: FetchSignInActionInterface):any {
  try { 
    yield put(setUserLoadingStatus(LoadingStatus.LOADING));
    //we getting information about authorized user according to data of authorized user payload
    const {data} = yield call(AuthApi.signIn, payload);
    //we creating token of authorized user
    window.localStorage.setItem('token', data.token);
    //we fixing data of authorized user
    yield put(setUserData(data));
  } catch (error) {
    //user authorization failed
    yield put(setUserLoadingStatus(LoadingStatus.ERROR));
  }
}

//the function for receiving data according to an authorized user if there is such a user
export function* fetchUserDataRequest():any {
  try {
    //the status of downloading information from server about authorized user 
    yield put(setUserLoadingStatus(LoadingStatus.LOADING));
    //receive information about authorized user 
    const {data} = yield call(AuthApi.getMe);  
    if (data !== undefined) 
    {
      //if there is information about authorized user then we fixing this information
      yield put(setUserData(data.data));
    }
    else
    {
      //if it no then we outputing status of error downloading information from server
      yield put(setUserLoadingStatus(LoadingStatus.ERROR));
    }
  } catch (error) {
    //to get data according to authorized user didn't work at all
    yield put(setUserLoadingStatus(LoadingStatus.ERROR));
  }
}

export function* userSaga() {
  //we serving data about new registered user according to sagas
  yield takeLatest(UserActionsType.FETCH_SIGN_UP, fetchSignUpRequest);
  //we serving data about authorized user according to sagas
  yield takeLatest(UserActionsType.FETCH_SIGN_IN, fetchSignInRequest);
  //the request for getting data about authorized user according to sagas
  yield takeLatest(UserActionsType.FETCH_USER_DATA, fetchUserDataRequest);
}