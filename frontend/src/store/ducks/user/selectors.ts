import { RootState } from '../../store';
import { UserState } from './contracts/state';

//we receiving full data about user from ./reducer.ts including status them forming
export const selectUserState = (state: RootState): UserState =>{ return state.user;}

//we receiving data about user
export const selectUserData = (state: RootState): UserState['data'] => selectUserState(state).data;

//we geting logical information whether there is an authorized user or not
export const selectIsAuth = (state: RootState): boolean => selectUserState(state).data !== undefined;

//we geting current status of forming data about user
export const selectUserStatus = (state: RootState): UserState['status'] => selectUserState(state).status;