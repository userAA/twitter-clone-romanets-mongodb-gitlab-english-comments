import {LoadingStatus} from '../../../types';

//interface according to data itself about user
export interface User {
  _id?: string;
  email: string;
  fullname: string;
  username: string;
  password: string;
  confirmHash: string;
  confirmed?: boolean;
  location?: string;
  about?: string;
  website?: string;
}

//interface according to data about user including status of them creating
export interface UserState {
  //data according to user
  data: User | undefined; 
  //the status of creating data according to user
  status: LoadingStatus;  
}