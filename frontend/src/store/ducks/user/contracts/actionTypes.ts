import {Action} from 'redux';
import { LoginFormProps } from '../../../../pages/SignIn/components/LoginModal';
import { RegisterFormProps } from '../../../../pages/SignIn/components/RegisterModal';
import {LoadingStatus} from '../../../types';
import {User} from './state';

export enum UserActionsType {
    //the type of receiving data about an authorized user
    SET_USER_DATA = 'user/SET_USER_DATA',          
    //the type of request for user authorization
    FETCH_SIGN_IN = 'user/FETCH_SIGN_IN',          
    //the type of request into server about user with token (authorized user)
    FETCH_USER_DATA = 'user/FETCH_USER_DATA',     
    //the type of request into server about registration of new user
    FETCH_SIGN_UP = 'user/FETCH_SIGN_UP',         
    //the type for getting status of creating data about user
    SET_LOADING_STATE = 'user/SET_LOADING_STATE',  
    //the type of removing authorization from the user, but it is on the server
    SIGN_OUT = 'user/SIGN_OUT'                     
}

//this interface talking about that data about user there are on server but there is no authorized user
export interface SignOutActionInterface extends Action<UserActionsType> {
    type: UserActionsType.SIGN_OUT;
}

//the interface of request according to authorization user
export interface FetchSignInActionInterface extends Action<UserActionsType> {
    type: UserActionsType.FETCH_SIGN_IN;
    payload: LoginFormProps;
}

//the interface of request about registration new user
export interface FetchSignUpActionInterface extends Action<UserActionsType> {
    type: UserActionsType.FETCH_SIGN_UP;
    payload: RegisterFormProps;
}

//interface of request of data about authorized user
export interface FetchUserDataActionInterface extends Action<UserActionsType> {
    type: UserActionsType.FETCH_USER_DATA;
}

//interface of receiving data of new authorized user
export interface SetUserDataActionInterface extends Action<UserActionsType> {
    type: UserActionsType.SET_USER_DATA;
    payload: User | undefined;
}

//user data generation status interface
export interface SetUserLoadingStatusActionInterface extends Action<UserActionsType> {
    type: UserActionsType.SET_LOADING_STATE;
    payload: LoadingStatus;
}