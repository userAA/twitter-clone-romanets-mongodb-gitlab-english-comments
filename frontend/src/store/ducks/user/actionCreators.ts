//user authorization form
import { LoginFormProps } from '../../../pages/SignIn/components/LoginModal';
//user registration form
import { RegisterFormProps } from '../../../pages/SignIn/components/RegisterModal';
import { 
  //this interface talking that data about user there are on server but no authorized user
  SignOutActionInterface,          
  //interface of request according to user authorization  
  FetchSignInActionInterface,           
  //interface of request according to registration new user
  FetchSignUpActionInterface,           
  //interface of request data about authorized user
  FetchUserDataActionInterface,         
  //interface of receiving data of new authorized user
  SetUserDataActionInterface,        
  //interface of status creating data about user 
  SetUserLoadingStatusActionInterface, 
  //the type of actions with data about user and statuses of them creating
  UserActionsType                       
} from './contracts/actionTypes';
import {UserState} from './contracts/state';

//we switching data about authorized user in reducer
export const setUserData = (payload: UserState['data']): SetUserDataActionInterface => ({
  //label sending in reducer.ts
  type: UserActionsType.SET_USER_DATA, 
  //data sending in reducer.ts
  payload, 
});

//we removing registration from user
export const signOut = (): SignOutActionInterface => ({
  //the label sending into reducer.ts
  type: UserActionsType.SIGN_OUT 
});

//the request for registration of new user
export const fetchSignUp = (payload: RegisterFormProps): FetchSignUpActionInterface => ({
  //according to this flag this request is passed into sagas.ts
  type: UserActionsType.FETCH_SIGN_UP, 
  payload
});

//the request for authorization of an already registered user
export const fetchSignIn = (payload: LoginFormProps): FetchSignInActionInterface => ({
  //according to this flag this request is passed into sagas.ts
  type: UserActionsType.FETCH_SIGN_IN,  
  payload,
});

//the request for getting information is there an authorized user or not
export const fetchUserData = (): FetchUserDataActionInterface => ({
  //according to this flag this request is passed into sagas.ts
  type: UserActionsType.FETCH_USER_DATA 
});

//we setting status of creating data about users
export const setUserLoadingStatus = (payload: UserState['status']): SetUserLoadingStatusActionInterface => ({
  //label in reducer.ts
  type: UserActionsType.SET_LOADING_STATE,
  payload,
});

//we combining interfaces according to user data
export type UserActions =
  | SetUserDataActionInterface
  | FetchSignInActionInterface
  | SetUserLoadingStatusActionInterface
  | FetchUserDataActionInterface
  | SignOutActionInterface;