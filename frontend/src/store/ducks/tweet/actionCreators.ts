import { LoadingStatus } from '../../types';
import {
  //interface for request about downloading tweet according to ID payload from server
  FetchTweetDataActionInterface,          
  //downloaded tweet data interface
  SetTweetDataActionInterface,            
  //interface of state of process downloading tweet from server
  SetTweetLoadingStatusActionInterface,   
  //type of actions with tweet data
  TweetActionsType,                       
} from './contracts/actionTypes';
import { TweetState } from './contracts/state';

//we sending tweet into store
export const setTweetData = (payload: TweetState['data']): SetTweetDataActionInterface => ({
  type: TweetActionsType.SET_TWEET_DATA,
  payload,
});

//we send into store the status of downloading tweet from server according to ID
export const setTweetLoadingStatus = (
  payload: LoadingStatus,
): SetTweetLoadingStatusActionInterface => ({
  type: TweetActionsType.SET_LOADING_STATE,
  payload,
});

//we sending into sagus the request for downloading tweet from server with ID payload
export const fetchTweetData = (payload: string): FetchTweetDataActionInterface => ({
  type: TweetActionsType.FETCH_TWEET_DATA,
  payload,
});

//combining intefaces of data about downloading tweet with server
export type TweetActions =
  | SetTweetDataActionInterface
  | FetchTweetDataActionInterface
  | SetTweetLoadingStatusActionInterface;
