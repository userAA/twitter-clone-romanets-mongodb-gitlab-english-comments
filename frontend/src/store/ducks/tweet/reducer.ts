import produce, { Draft } from 'immer';
import { LoadingStatus } from '../../types';
import { TweetActions } from './actionCreators';
import { TweetActionsType } from './contracts/actionTypes';
import { TweetState } from './contracts/state';

//initial state
const initialTweetState: TweetState = {
  //there is no tweet downloaded from the server
  data: undefined,               
  //the tweet was not downloaded from the server
  LoadingStatus: LoadingStatus.NEVER 
};

export const tweetReducer = produce((draft: Draft<TweetState>, action: TweetActions) => {
  switch (action.type) {
    //tweet downloaded акщь server according to the given ID and located in store, is being taken from store
    case TweetActionsType.SET_TWEET_DATA:
      draft.data = action.payload;
      //status, talking about that the tweet was downloaded from the server
      draft.LoadingStatus = LoadingStatus.LOADED; 
      break;

    //tweet is being downloaded from server according to known ID
    case TweetActionsType.FETCH_TWEET_DATA:
      draft.data = undefined;
      //the state, talking about tweet is being downloaded from server 
      draft.LoadingStatus = LoadingStatus.LOADING;
      break;

    //we fixing progress of process of downloading tweet according to known ID from server
    case TweetActionsType.SET_LOADING_STATE:
      draft.LoadingStatus = action.payload;
      break;

    default:
      break;
  }
}, initialTweetState);