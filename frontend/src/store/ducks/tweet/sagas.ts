import { call, put, takeEvery } from 'redux-saga/effects';
import { TweetsApi } from '../../../services/api/tweetsApi';
import { LoadingStatus } from '../../types';
import { Tweet } from '../tweets/contracts/state';
import { setTweetData, setTweetLoadingStatus } from './actionCreators';
import { FetchTweetDataActionInterface, TweetActionsType } from './contracts/actionTypes';

//the function of implementing a request to download a tweet by its identifier from the server
export function* fetchTweetDataRequest({ payload: tweetId }: FetchTweetDataActionInterface):any {
  try {
    //we downloading tweet from server according to ID tweetId
    const data: Tweet = yield call(TweetsApi.fetchTweetData, tweetId);
    //fixing downloaded tweet from server
    yield put(setTweetData(data));
  } catch (error) {
    //to download tweet from server according to ID tweetId didn't work out
    yield put(setTweetLoadingStatus(LoadingStatus.ERROR));
  }
}

export function* tweetSaga() {
  //making request for downloading tweet from server according to available ID
  yield takeEvery(TweetActionsType.FETCH_TWEET_DATA, fetchTweetDataRequest);
}