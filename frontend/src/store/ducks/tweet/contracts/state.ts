import { LoadingStatus } from '../../../types';
import { Tweet } from '../../tweets/contracts/state';

//interface of full state tweet
export interface TweetState {
    //tweet data
    data?: Tweet;  
    //the state of downloafing tweet data with server
    LoadingStatus: LoadingStatus; 
}
