import { Action } from 'redux';
import { LoadingStatus } from '../../../types';
import { TweetState } from './state';

export enum TweetActionsType {
    //type of location of downloaded from server tweet in store
    SET_TWEET_DATA = 'tweet/SET_TWEET_DATA',       
    //type of request for downloading tweet from server
    FETCH_TWEET_DATA = 'tweet/FETCH_TWEET_DATA', 
    //type of process states of downloading tweet with server  
    SET_LOADING_STATE = 'tweet/SET_LOADING_STATE', 
}

//downloaded tweet data interface
export interface SetTweetDataActionInterface extends Action<TweetActionsType> {
    type: TweetActionsType.SET_TWEET_DATA;
    payload: TweetState['data'];
}

//interface for request about downloading tweet according to ID payload from server
export interface FetchTweetDataActionInterface extends Action<TweetActionsType> {
    type: TweetActionsType.FETCH_TWEET_DATA;
    payload: string;
}

//interface of process state of downloading tweet from server
export interface SetTweetLoadingStatusActionInterface extends Action<TweetActionsType> {
    type: TweetActionsType.SET_LOADING_STATE;
    payload: LoadingStatus;
}
