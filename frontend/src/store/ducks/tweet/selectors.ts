
import {RootState} from "../../store";
import { Tweet } from '../tweets/contracts/state';
import {LoadingStatus} from '../../types';
import { TweetState} from "./contracts/state";

//taking from reducer downloaded tweet according to known ID
export const selectTweet = (state: RootState): TweetState => state.tweet;

//tracking according to reducer the process of downloading tweet according to known ID
export const selectLoadingState = (state: RootState): LoadingStatus => 
    selectTweet(state).LoadingStatus;

//we checking with the reducer whether the tweet is downloaded by the specified identifier
export const selectIsTweetLoading = (state: RootState): boolean => 
    selectLoadingState(state) === LoadingStatus.LOADING;
 
//we taking from reducer according to store the data of downloaded tweet from server 
//according to known ID if there is such a tweet
export const selectTweetData = (state: RootState): Tweet | undefined => 
    selectTweet(state).data;