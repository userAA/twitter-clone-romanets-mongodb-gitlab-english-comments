import TwitterIcon from '@material-ui/icons/Twitter';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch, useHistory } from 'react-router-dom';

import { Home } from './pages/Home';
import { Layout } from './pages/Layout';
import { SignIn } from './pages/SignIn';
import { useHomeStyles } from './pages/theme';
import { UserPage } from './pages/User/index';
import { fetchUserData } from './store/ducks/user/actionCreators';
import { selectIsAuth, selectUserStatus} from './store/ducks/user/selectors';
import { LoadingStatus } from './store/types';

function App() {

  const dispatch = useDispatch();

  React.useEffect(() => {
    //we making up request for information about authorized user according to token
    dispatch(fetchUserData());
  }, [dispatch]);

  const classes = useHomeStyles();
  //hook of transmition accross web pages
  const history = useHistory();

  //flag there is authorized user according to token or no
  const isAuth = useSelector(selectIsAuth);

  //the status of creating information about user
  const loadingStatus = useSelector(selectUserStatus);
   
  //isReady talk about, that information about user created but it may be error ()
  const isReady = loadingStatus !== LoadingStatus.NEVER && 
                  loadingStatus !== LoadingStatus.LOADING;

  React.useEffect(() => {
    if ((!isAuth) || loadingStatus === LoadingStatus.ERROR )   {
      //we returning into page of authorization and registration when there is no authorized user
      history.push('/signin');
    } else if (isAuth && history.location.pathname === '/signin') {
      //we switching into page of tweets if there is authorized user
      history.push('/home');
    }
  }, [isAuth,history,loadingStatus]);

  //the bird is being shown when going process of creating information about user on server
  //or when the user is being authenticated on the server
  //or when going the process of obtaining information about an authorized user from the server 
  if (!isReady) {
    return (
      <div className={classes.centered}>
        <TwitterIcon color="primary" style={{ width: 80, height: 80 }} />
      </div>
    )
  }
  
  return (
    <div className="App">  
      <Switch>
        {/*Authorization and registration page */}
        {!isAuth && <Route path="/signin" component={SignIn} exact /> } 
        <Layout>
          {/*The page of creating tweets user */}
          <Route path="/home" component={Home} />
          {/*The page of authorized user*/}
          <Route path="/user/:id" component={UserPage} exact/>
        </Layout>
      </Switch>
    </div>
  );
}

export default App;