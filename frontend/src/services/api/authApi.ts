import { axios } from '../../core/axios';
//user authorization data form
import { LoginFormProps } from '../../pages/SignIn/components/LoginModal';
//user registration data form
import { RegisterFormProps } from '../../pages/SignIn/components/RegisterModal';

interface ResponseApi {
    status: string;
    data: any;
}

export const AuthApi = {
    //request into server for registration of new user
    async signIn(postData: LoginFormProps): Promise<ResponseApi> {
        const { data } = await axios.post<ResponseApi>('/auth/login', { username: postData.email, password: postData.password });
        return data;
    },
    //request into server for user authorization
    async signUp(postData: RegisterFormProps): Promise<ResponseApi> {
        const { data } = await axios.post<ResponseApi>('/auth/register', { email: postData.email, username: postData.username, fullname: postData.fullname, password: postData.password, password2: postData.password2 });
        return data;
    }, 
    //request into server for receiving information about authorized user, if in localStorage
    //exists token, overwise authorized user does not exist
    async getMe(): Promise<ResponseApi> {
        const { data } = await axios.get<ResponseApi>('/users/me');
        return data;
    },
    //request for receiving of full information about user witf ID userId including tweets of this user
    async getUserInfo(userId: string): Promise<ResponseApi> {
        const { data } = await axios.get<ResponseApi>('/users/' + userId);
        return data;
    },
};

// @ts-ignore
window.AuthApi = AuthApi;
