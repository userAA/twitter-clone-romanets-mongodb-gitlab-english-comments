import {axios} from '../../core/axios';
import {Tweet} from '../../store/ducks/tweets/contracts/state';

interface Response<T> {
  status: string;
  data: T;
}

export const TweetsApi = {

  //requst into server for downloading from server of tweets. If ID of authorized user is defined then with server
  //are being downloaded all tweets according to authorized user, overwise with server absolutely 
  //all tweets are being downloaded
  async fetchTweets(userId?: string): Promise<Tweet[]> {
    const {data} = await axios.get<Response<Tweet[]>>(userId ? `/tweets/user/${userId}` : '/tweets');
    return data.data;
  },

  //the request into server for downloading with it of tweet with ID id
  async fetchTweetData(id: string): Promise<Tweet> {
    const {data} = await axios.get<Response<Tweet>>('/tweets/'+id);
    return data.data;
  },

  //request into server for adding of new tweet according to authorized user 
  async addTweet(payload: {text: string; images: string[]}): Promise<Tweet> {
    const {data} = await axios.post<Response<Tweet>>('/tweets', payload);
    return data.data;
  },

  //request for removing from server of tweet according to it ID
  removeTweet: (id: string): Promise<void> => axios.delete('/tweets/' + id),
};